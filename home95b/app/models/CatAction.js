const mongoose = require("mongoose");
const idvalidator = require("mongoose-id-validator");
const Schema = mongoose.Schema;

const CatActionSchema = new Schema(
    {
        catName: {
            type: String,
            required: [true, props => {
                return `Поле ${props.path} должно быть заполнено`
            }],
        },
        catOld: {
            type: String,
            required: [true, props => {
                return `Поле ${props.path} должно быть заполнено`
            }],
        },
        eatLevel: {
            type: Number,
            default: 20,
        },
        happyLevel: {
            type: Number,
            default: 50,
        },
        avatarCat: {
            type: String,
            default: 'Qgk3C4ctY_RH5DuccYWv8.jpeg',
        },
        catAction: {
            type: String,
            enum: ['sleep', 'noSleep'],
            default: 'noSleep',
        },
        datetime: {
            type: Date,
            default: Date.now(),
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
    },
    {
        versionKey: false
    }
);

CatActionSchema.plugin(idvalidator, {
    message: 'Bad ID value for {PATH}'
})
const CatAction = mongoose.model('Cataction', CatActionSchema);
module.exports = CatAction;