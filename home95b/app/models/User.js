const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const { nanoid } = require('nanoid');

const SALT_WORK_FACTOR = 10;
const Schema = mongoose.Schema;

const UserShema = new Schema(
    {
        username: {
            type: String,
            unique: true,
            required: [true, props => {
                console.log(props);
                return `Поле ${props.path} должно быть заполнено`;
            }],
            validate: {
                validator: async value => {
                    const user = await User.findOne({ username: value });
                    if (user) return false;
                },
                message: 'Пользователь с таким логином уже зарегистрирован!'
            }
        },
        displayName: {
            type: String,
            required: true,
        },
        password: {
            type: String,
            required: [true, props => {
                console.log(props);
                return `Поле ${props.path} должно быть заполнено`;
            }],
            // validate: {
            //     validator: value => {
            //         const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
            //         if (regex.test(value)) return false;
            //     },
            //     message: 'Введите более сложный пароль',
            // }
        },
        email: {
            type: String,
            required: [true, props => {
                console.log(props);
                return `Поле ${props.path} должно быть заполнено`
            }],
            validate: [
                {
                    validator: (value) => {
                        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                        if (!regex.test(value)) return false;
                    },
                    message: 'Вы ввели неправильный email'
                },
                {
                    validator: async (value) => {
                        const user = await User.findOne({ email: value });
                        if (user) return false;
                    },
                    message: 'Такое email уже зарегистрирован'
                }]
        },
        avatarImage: String,
        token: {
            type: String,
            required: true,
        },
        role: {
            type: String,
            required: true,
            enum: ['user', 'admin'],
            default: 'user',
        },
        facebookId: {
            type: Number,
            minlength: 10,
        }
    },
    {
        versionKey: false
    }
)

UserShema.pre("save", async function (next) {
    if (!this.isModified("password")) next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR)
    const hash = await bcrypt.hash(this.password, salt);
    this.password = hash;
    next();
})

UserShema.set("toJSON", {
    transform: (doc, ret) => {
        delete ret.password;
        return ret;
    }
})

UserShema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password);
}

UserShema.methods.generateToken = function () {
    this.token = nanoid();
}

const User = mongoose.model('User', UserShema);

module.exports = User;