const express = require('express');

const router = express.Router();
const User = require('./models/User');
const auth = require('./middleware/auth');
const config = require('./config');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const axios = require('axios');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});
const upload = multer({ storage });

const createRouter = () => {
    router.post('/', upload.single('avatarImage'), async (req, res) => {
        try {
            const user = new User({
                username: req.body.username,
                displayName: req.body.displayName,
                password: req.body.password,
                email: req.body.email,
            });
            if (req.file) {
                user.avatarImage = req.file.filename;
            }
            user.generateToken();
            await user.save();
            res.send(user);
        } catch (e) {
            res.status(400).send(e);
        }
    });

    router.post("/sessions", async (req, res) => {
        const error = "Username or password are wrong"
        const user = await User.findOne({ username: req.body.username });
        if (!user) {
            return res.status(400).send({ error })
        }
        const isMatch = await user.checkPassword(req.body.password);
        if (!isMatch) {
            return res.status(400).send({ error });
        }

        user.generateToken();
        const userCopy = {
            id: user._id,
            token: user.token,
            displayName: user.displayName,
            avatarImage: user.avatarImage,
        }
        await user.save({ validateBeforeSave: false });
        res.send(userCopy)
    });

    router.delete("/sessions", auth, async (req, res) => {
        const user = req.user;
        const success = { message: 'Success logout' }
        user.token = '';

        await user.save({ validateBeforeSave: false });
        res.send(success);
    });

    router.post("/facebooklogin", async (req, res) => {
        const inputToken = req.body.accessToken;
        const accessToken = `${config.facebook.appId}|${config.facebook.secret}`;
        const tokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
        try {
            const response = await axios.get(tokenUrl);
            if (response.data.error) {
                return res.status(401).send({ message: 'Facebook token error' });
            }
            if (response.data.data.user_id !== req.body.userID) {
                return res.status(401).send({ message: 'Wrong User Id' });
            }
            let user = await User.findOne({ facebookId: req.body.id })
            if (!user) {
                user = new User({
                    displayName: req.body.email,
                    username: req.body.name,
                    email: req.body.email,
                    password: nanoid(),
                    facebookId: req.body.id,
                })
                if (req.body.picture.data.url) {
                    user.avatarImage = req.body.picture.data.url;
                }
            }

            user.generateToken();
            await user.save({ validateBeforeSave: false });
            return res.send({ message: 'Facebook login or register successfull', user });
        } catch (e) {
            return res.status(401).send({ message: 'Facebook token error' });
        }
    });
    return router;
}


module.exports = createRouter;