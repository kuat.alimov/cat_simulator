const express = require('express');
const router = express.Router();

const auth = require('./middleware/auth');
const permit = require('./middleware/permit');

const CatAction = require('./models/CatAction');
const User = require('./models/User');


const createRouter = () => {
    router.get('/', [auth, permit('admin', 'user')], async (req, res) => {
        let queryFilter = {};
        if (req.query.userId) {
            queryFilter.userId = req.query.userId;
        }
        try {
            const user = await User.findById(queryFilter.userId);
            if (!user) {
                return res.status(404).send({ message: 'User not found' })
            }
            const result = await CatAction.find({ user: user._id }).populate('user', '_id username displayName email avatarImage');

            res.send(result);
        } catch (e) {
            res.status(400).send(e);
        }
    });

    router.post('/', [auth, permit('admin', 'user')], async (req, res) => {
        try {
            const result = new CatAction(req.body);
            result.user = req.user._id;
            await result.save();
            res.send(result);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.post('/cat-action', [auth, permit('admin', 'user')], async (req, res) => {
        let cat = {};
        if (req.body.catPlaySelect && req.body.catPlaySelect !== '') {
            cat.action = req.body.catPlaySelect;
            cat.id = req.body.catID;
        } else {
            return res.status(400).send({ message: 'Выберите действие' });
        }
        try {
            const result = await CatAction.findById({ _id: cat.id }).populate('user', '_id username displayName email avatarImage');
            const lastTimeEat = new Date(result.datetime);
            if (cat.action === 'покормить кота') {
                if (result.catAction === 'sleep') {
                    return res.status(400).send({ errMssg: 'Кот спит!', result });
                }
                if (result.eatLevel >= 100) {
                    result.catAction = 'sleep';
                    await result.save();
                    return res.status(400).send({ errMssg: 'Кот спрятался и спит!', result });
                }

                if (result.eatLevel <= 100) {
                    result.eatLevel = result.eatLevel + 15;
                    if (result.happyLevel <= 100) {
                        result.happyLevel = result.happyLevel + 5;
                    }

                    result.datetime = new Date().toISOString();
                } else {
                    if (result.eatLevel <= 100) {
                        result.happyLevel = result.happyLevel - 30;
                        if (result.happyLevel <= 0) {
                            result.happyLevel = 0;
                        }
                    }
                    const newDate = new Date().getDate();
                    const checkDate = newDate - lastTimeEat.getDate();
                    const newHours = new Date().getHours();
                    const checkHours = newHours - lastTimeEat.getHours();

                    if (checkDate >= 0 && checkDate <= 1) {
                        if (checkHours >= 3) {
                            result.eatLevel = result.eatLevel - 40;
                        }
                        return res.status(400).send({ errMssg: 'По кормить Кота можно через 3 часа ', result });
                    }
                }
            }
            if (cat.action === 'уложить спать кота') {
                if (result.catAction === 'sleep') {
                    return res.status(400).send({ errMssg: 'Кот уже спит!', result });
                }
                result.catAction = 'sleep';
            }
            if (cat.action === 'поиграть с котом') {
                if (result.happyLevel === 0) {
                    const newDate = new Date().getDate();
                    const checkDate = newDate - lastTimeEat.getDate();
                    const newHours = new Date().getHours();
                    const checkHours = newHours - lastTimeEat.getHours();
                    const newTime = new Date().getMinutes();
                    const checkTime = newTime - lastTimeEat.getMinutes();
                    if (checkDate >= 0 && checkDate <= 1) {
                        if (checkHours >= 0 || checkTime >= 1) {
                            result.happyLevel = 30;
                            await result.save();
                            return res.status(400).send({ errMssg: 'Кот успокоился!', result });
                        }
                    }
                    return res.status(400).send({ errMssg: 'Кот в бешенстве!', result });
                }
                if (result.catAction === 'sleep') {
                    result.catAction = 'noSleep';
                    if (result.eatLevel >= 100) {
                        result.catAction = 'sleep';
                        const newDate = new Date().getDate();
                        const checkDate = newDate - lastTimeEat.getDate();
                        const newHours = new Date().getHours();
                        const checkHours = newHours - lastTimeEat.getHours();
                        const newTime = new Date().getMinutes();
                        let checkTime = newTime - lastTimeEat.getMinutes();
                        if (checkTime < 0) checkTime = -1 * checkTime;
                        if (checkDate >= 0 && checkDate <= 1) {
                            if (checkHours >= 0 && checkTime >= 1) {
                                result.happyLevel = 50;
                                result.eatLevel = 40;
                                result.catAction = 'noSleep';
                                await result.save();
                                return res.status(400).send({ errMssg: 'Кот проснулся!', result });
                            }
                        }

                        await result.save();
                        return res.status(400).send({ errMssg: 'Кот спрятался и спит!', result });
                    }
                    if (result.happyLevel > 0 && result.happyLevel <= 130) {
                        if (result.catAction === 'noSleep') {
                            result.happyLevel = result.happyLevel - 5;
                        }
                    }
                }
                const min = 1;
                const max = 3;
                const catHappyIndex = Math.floor(Math.random() * (max - min + 1)) + min;
                if (catHappyIndex !== 1) {
                    if (result.happyLevel <= 100) {
                        result.happyLevel = result.happyLevel + 15;
                    } else {
                        result.happyLevel = 30;
                        await result.save();

                        return res.status(400).send({ errMssg: 'Кот спрятался и раздаржен!', result });
                    }
                    if (result.eatLevel <= 100 && result.eatLevel > 15) {
                        result.eatLevel = result.eatLevel - 10;
                    }
                    if (result.eatLevel <= 10) {
                        result.happyLevel = 10;
                        await result.save();

                        return res.status(400).send({ errMssg: 'Кот голоден и раздаржен!', result });
                    }
                } else {
                    result.happyLevel = 0;
                    await result.save();
                    return res.status(400).send({ errMssg: 'Кот в бешенстве!', result });
                }
            }

            await result.save();
            res.send(result);
        } catch (e) {
            res.status(400).send(e);
        }
    });
    return router;
}


module.exports = createRouter;

// router.post('/:id/guest', [auth, permit('admin', 'user')], async (req, res) => {
//     const result = await EventsDay.findById(req.params.id);
//     const user = await User.findOne({ email: result.email });
//     if (!user) {
//         return res.status(404).send({ message: 'User not found' })
//     }
//     result.user = user._id;
//     try {
//         await result.save();
//         res.send(result);
//     } catch (e) {
//         res.status(500).send(e);
//     }
// });


    // router.delete('/:id', [auth, permit('admin', 'user')], async (req, res) => {
    //     try {
    //         await EventsDay.findByIdAndDelete({ _id: req.params.id });
    //         const messageDelete = { message: 'Events Day delete!' }
    //         res.send(messageDelete);
    //     } catch (e) {
    //         res.status(500).send(e);
    //     }
    // });