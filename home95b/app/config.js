const path = require('path');
const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, '../public/uploads'),
    db: {
        name: 'catsimulator',
        url: "mongodb://localhost"
    },
    facebook: {
        appId: "416283772962264",
        secret: "43c7b2f7007fe90f11eb93041342a2bd",
    }
}