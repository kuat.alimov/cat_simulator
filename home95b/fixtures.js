const config = require('./app/config');
const mongoose = require('mongoose');

const CatAction = require('./app/models/CatAction');
const User = require('./app/models/User');

const { nanoid } = require("nanoid");

mongoose.connect(config.db.url + '/' + config.db.name)
const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('catactions');
        await db.dropCollection('users');
    } catch (e) {
        console.log('Collections not found');
    }

    const [admin, userMike] = await User.create(
        {
            username: 'admin',
            displayName: 'admin',
            email: "admin_FM@rambler.ru",
            password: "123456",
            role: 'admin',
            avatarImage: 'Z1B1bK4FateCullXFSzw8.jpeg',
            token: nanoid(),
        },
        {
            username: "mike_prod",
            displayName: "Mike Kori",
            email: "mike_prod@gmail.com",
            password: "123456",
            role: "user",
            avatarImage: "H_7vPeEZc1VfhpnNBZ1i0.jpg",
            token: nanoid(),
        },
    )

    await CatAction.create(
        {
            user: userMike._id,
            catName: 'Mark',
            catOld: '1',
        },
    )

    await db.close();
});
