const express = require('express');
const cors = require('cors')
const app = express();
const port = 8000;
const config = require('./app/config');

const mongoose = require('mongoose');
const catsimulator = require('./app/catSimulator');
const user = require('./app/user');


const corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions));
app.use(express.json())
app.use(express.static('public'));

const run = async () => {

    await mongoose.connect(config.db.url + '/' + config.db.name, { useNewUrlParser: true })
    app.use('/catsimulator', catsimulator());
    app.use('/users', user());

    app.listen(port, () => {
        console.log(`Server started on port: ${port}`);
    });
};


run().catch(err => {
    console.error(err);
})