import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, Link } from 'react-router-dom';
import { AppBar, Menu, Button, MenuItem, Fade, makeStyles, Typography, Toolbar, Avatar } from "@material-ui/core";
import { logoutUser } from '../../store/action/usersAction';
import { apiURL } from '../../apiUrl';

const useStyles = makeStyles(theme => (
    {
        mainLink: {
            color: 'inherit',
            textDecoration: 'none',
            '&:hover': {
                color: 'inherit',
            },
            fontFamily: 'Arial, sans-serif',
        },
        linkHistory: {
            color: 'white',
            textDecoration: 'none',
            '&:hover': {
                color: 'gray',
            },
            margin: theme.spacing(1, 1.5),
        },
        staticToolbar: {
            marginBottom: theme.spacing(2),
        },
        link: {
            margin: theme.spacing(1, 1.5),
            fontFamily: 'Arial, sans-serif',
        },
        toolbar: {
            flexWrap: 'wrap'
        },

        toolbarTitle: {
            flexGrow: 1,
        },
        navigation: {
            marginBottom: theme.spacing(2),
        },
        avatar: {
            margin: theme.spacing(1),
        },

    }
))

const Header = (props) => {
    const dispatch = useDispatch();

    const user = useSelector(state => state.users.user)
    const classes = useStyles();
    const [avatarIMG, setAvatarImg] = useState('');
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const logoutUserHandler = () => {
        setAnchorEl(null);
        if (user !== null) {
            dispatch(logoutUser());
        }
    };
    useEffect(() => {
        if (user) {
            if (user.facebookId) {
                setAvatarImg(user.avatarImage)
            } else {
                setAvatarImg(`${apiURL}/uploads/${user.avatarImage}`)
            }
        }
    })
    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Typography variant="h6" className={classes.toolbarTitle}>
                        {user ? null : <Link to="/" className={classes.mainLink}>MAIN PAGE</Link>}
                    </Typography>
                    {user ? (
                        <>
                            {user.avatarImage ? <Avatar className={classes.avatar} src={avatarIMG} /> : null}
                            <Button color='inherit' aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                                Hello, {user.displayName}
                            </Button>
                            <Menu
                                id="fade-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={open}
                                onClose={handleClose}
                                TransitionComponent={Fade}
                            >
                                <MenuItem onClick={handleClose} >Profile</MenuItem>
                                <MenuItem onClick={handleClose} >My account</MenuItem>
                                <MenuItem onClick={logoutUserHandler}>Logout</MenuItem>
                            </Menu>
                        </>
                    ) : (
                            <>
                                <Button to="/register" color='inherit'
                                    variant="outlined" className={classes.link} component={NavLink}>
                                    Sign up
                                </Button>
                                <Button to="/login" color='inherit'
                                    variant="outlined" className={classes.link} component={NavLink}>
                                    Sign In
                                </Button>
                            </>
                        )}
                </Toolbar>
            </AppBar>
            <Toolbar className={classes.staticToolbar} />
        </>
    )
}

export default Header;