import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Grid, Button, Container } from '@material-ui/core';

import FormElement from '../Form/FormElement';
import Spinner from '../Spinner/Spinner';
import BackDrop from '../BackDrop/Backdrop';
import { friendEventDay } from '../../../store/action/calendarAction';

const AddFriend = () => {

    const dispatch = useDispatch();

    const user = useSelector(state => state.users.user)

    const errors = useSelector(state => state.calendarEvent.error);

    const [loading, setLoading] = useState(false);

    const [state, setState] = useState({
        email: '',
    });

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setState(prevState => {
            return {
                ...prevState,
                [name]: value,
            }
        }
        )
    }

    const onFormSubmit = async (dataCopy) => {
        setLoading(true)
        await dispatch(friendEventDay(dataCopy));
        setState({ email: "Ваши события расшарены" })
        setLoading(false)
    }

    const submitFormHandler = (event) => {
        event.preventDefault();
        if (state.email !== '') {
            const dataCopy = {
                email: state.email,
            }
            onFormSubmit(dataCopy);
        } else {
            alert("Field email empty");
        }
    }

    const getFieldError = () => {
        try {
            return errors.message;
        } catch {
            return undefined;
        }
    }


    return (
        <>
            {loading && <>
                <BackDrop show={loading} />
                <Spinner />
            </>
            }
            <Container maxWidth="sm" component="main">
                <form autoComplete="off" onSubmit={submitFormHandler}>
                    <Grid container direction="row" spacing={2}>
                        <Grid item>
                            <FormElement
                                type="text"
                                label="Email friend"
                                id="email"
                                value={state.eventsDay}
                                onChange={inputChangeHandler}
                                name="email"
                                error={getFieldError()}
                                required={true} />
                        </Grid>
                        <Grid item>
                            <Button variant="outlined" type="submit" color="primary">Add friend</Button>
                        </Grid>
                    </Grid>
                </form>
            </Container>

        </>
    );
};

export default AddFriend;