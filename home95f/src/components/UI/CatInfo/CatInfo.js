import React from 'react';
import { useSelector } from 'react-redux';
import { Grid, makeStyles } from '@material-ui/core';
import { apiURL } from '../../../apiUrl';

const useStyles = makeStyles(theme => (
    {
        blockCatInfo: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        blockInfo: {
            margin: '0 auto'
        },
        infoText: {
            margin: theme.spacing(1),
        },
        blockBtn: {
            display: 'flex',
            flexDirection: 'column',
            margin: theme.spacing(1),
        },
        btnSpacing: {
            margin: theme.spacing(1),
        },
        imgWrapp: {
            display: 'flex',
            flexShrink: 0,
            width: '200px',
            height: '200px',
            justifyContent: 'center',
        },
        imgCat: {
            display: 'block',
            objectFit: 'contain',
            width: '90%'
        }
    }
));

const CatInfo = (props) => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user)

    return (
        <>
            <Grid item>
                <div className={classes.blockCatInfo}>
                    <div className={classes.imgWrapp}>
                        <img className={classes.imgCat} src={`${apiURL}/uploads/${props.avatarCat}`} />
                    </div>
                    <div className={classes.blockInfo}>
                        <p className={classes.infoText}><b>Cat name: </b>{props.catName}</p>
                        <p className={classes.infoText}><b>Cat old: </b>{props.catOld}</p>
                        <p className={classes.infoText}><b>Cat hungry: </b>{props.eatLevel}</p>
                        <p className={classes.infoText}><b>Cat happy: </b>{props.happyLevel}</p>
                    </div>
                </div>
            </Grid>
        </>
    )
}

export default CatInfo;