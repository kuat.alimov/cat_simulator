import React, { useState, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Grid, Button, Container, makeStyles } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import FormElement from '../Form/FormElement';
import { fetchActionWithCat, fetchCatAction } from '../../../store/action/catAction';

const useStyles = makeStyles(theme => (
    {
        selectAction: {
            width: '230px'
        }
    }
));

const PlayMenu = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const errors = useSelector(state => state.catAction.error);

    const optionSelect = useMemo(() => {
        return [
            { id: 1, name: 'покормить кота' },
            { id: 2, name: 'поиграть с котом' },
            { id: 3, name: 'уложить спать кота' }
        ]
    })

    const [state, setState] = useState({
        catPlaySelect: '',
    });

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setState(prevState => {
            return {
                ...prevState,
                [name]: value,
            }
        }
        )
    }

    const onFormSubmit = async (dataCopy) => {
        // console.log(dataCopy);
        await dispatch(fetchActionWithCat(dataCopy));
        // await dispatch(fetchCatAction(props.userID));
    }

    const submitFormHandler = (event) => {
        event.preventDefault();
        const dataCopy = {
            catPlaySelect: state.catPlaySelect,
            catID: props.catID,
        }
        onFormSubmit(dataCopy);
    }

    const getFieldError = (fieldName) => {
        try {
            // debugger
            return errors.message;
        } catch {
            return undefined;
        }
    }



    return (
        <>
            <Container maxWidth="sm" component="main">
                <Grid container direction="column" spacing={2} className={classes.selectAction}>
                    {errors && errors.errMssg && <Alert
                        className={classes.alertW}
                        severity="warning"
                    >{errors.errMssg}</Alert>}
                </Grid>
                <form autoComplete="off" noValidate onSubmit={submitFormHandler}>
                    <Grid container direction="column" spacing={2} className={classes.selectAction}>
                        <FormElement
                            label="Выберите действие"
                            id="catPlaySelect"
                            select
                            options={optionSelect}
                            value={state.catPlaySelect}
                            onChange={inputChangeHandler}
                            name="catPlaySelect"
                            error={getFieldError("catPlaySelect")}
                            required={true} />
                        <Grid item>
                            <Button variant="contained" type="submit" color="primary">Подтвердить</Button>
                        </Grid>
                    </Grid>
                </form>
            </Container>
        </>
    );
};

export default PlayMenu;