import { Button, Grid, makeStyles, TextField } from '@material-ui/core';
import React, { useState, useRef } from 'react';

const useStyle = makeStyles({
    input: {
        display: 'none',
    }
})

const FileInput = ({ onChange, name, label }) => {
    const inputRef = useRef();
    const classes = useStyle();
    const [filename, setFilename] = useState('');

    const onFileChange = (event) => {
        if (event.target.files[0]) {
            setFilename(event.target.files[0].name)
        } else {
            setFilename('')

        }
        onChange(event)
    }

    const activateInput = () => {
        inputRef.current.click();
    }

    return (
        <>
            <input
                type="file"
                name={name}
                className={classes.input}
                onChange={onFileChange}
                ref={inputRef}
            />
            <Grid container direction="row" spacing={2}>
                <Grid item>
                    <TextField
                        variant="outlined"
                        fullWidth
                        label={label}
                        value={filename}
                        disabled
                        onClick={activateInput}
                    />
                </Grid>
                <Grid item>
                    <Button variant="contained" onClick={activateInput}>Browse</Button>
                </Grid>
            </Grid>
        </>
    );
};

export default FileInput;