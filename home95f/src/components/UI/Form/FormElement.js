import React from 'react';
import { TextField, MenuItem, Grid } from '@material-ui/core/';
import PropTypes from 'prop-types';

const FormElement = ({ name, label, type, options, onChange, select, multiline, error, required, value, rows, InputProps }) => {

    let inputChildren = null;
    if (select) {
        inputChildren = options.map((option) => (
            <MenuItem key={option.id} value={option.name}>
                {option.name}
            </MenuItem>
        ))
    }

    return (
        <Grid item xs={12}>
            <TextField
                variant="outlined"
                fullWidth
                autoFocus
                multiline={multiline}
                rows={rows}
                select={select}
                autoComplete={name}
                name={name}
                type={type}
                required={required}
                id={name}
                label={label}
                value={value}
                onChange={onChange}
                InputProps={InputProps}
                error={!!error}
                helperText={error}
            >{inputChildren}</TextField>
        </Grid>
    );
};

FormElement.propTypes = {
    name: PropTypes.string.isRequired,
    type: PropTypes.string,
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    required: PropTypes.bool,
    multiline: PropTypes.bool,
    options: PropTypes.arrayOf(PropTypes.object),
    select: PropTypes.bool,
    rows: PropTypes.number,
    InputProps: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    error: PropTypes.string,
}

export default FormElement;