import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Grid, Button, Container } from '@material-ui/core';
import FormElement from '../Form/FormElement';
import Spinner from '../Spinner/Spinner';
import BackDrop from '../BackDrop/Backdrop';
import { addNewCat } from '../../../store/action/catAction';

const AddCat = () => {

    const dispatch = useDispatch();

    const errors = useSelector(state => state.catAction.error);

    const [loading, setLoading] = useState(false);

    const [state, setState] = useState({
        catName: '',
        catOld: '0',
    });

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setState(prevState => {
            return {
                ...prevState,
                [name]: value,
            }
        }
        )
    }

    const onFormSubmit = async (dataCopy) => {
        setLoading(true)
        console.log(dataCopy);
        await dispatch(addNewCat(dataCopy));
        setLoading(false)
    }

    const submitFormHandler = (event) => {
        event.preventDefault();
        const dataCopy = {
            catName: state.catName,
            catOld: state.catOld,
        }
        onFormSubmit(dataCopy);
    }

    const getFieldError = (fieldName) => {
        try {
            // debugger
            return errors.errors[fieldName].message;
        } catch {
            return undefined;
        }
    }

    return (
        <>
            {loading && <>
                <BackDrop show={loading} />
                <Spinner />
            </>
            }
            <Container maxWidth="sm" component="main">
                <form autoComplete="off" onSubmit={submitFormHandler}>
                    <Grid container direction="column" spacing={2}>
                        <h3>Create cat</h3>
                        <FormElement
                            type="text"
                            label="Cat name"
                            id="catName"
                            value={state.catName}
                            onChange={inputChangeHandler}
                            name="catName"
                            error={getFieldError("catName")}
                            required={true} />
                        <FormElement
                            type="number"
                            label="Cat old"
                            id="catOld"
                            value={state.catOld}
                            onChange={inputChangeHandler}
                            name="catOld"
                            error={getFieldError("catOld")}
                            required={true} />
                        <Grid item>
                            <Button variant="contained" type="submit" color="primary">Save</Button>
                        </Grid>
                    </Grid>
                </form>
            </Container>
        </>
    );
};

export default AddCat;