import React from 'react';
import { Grid, Button, makeStyles } from '@material-ui/core';
import { NavLink } from 'react-router-dom';

const useStyles = makeStyles(theme => (
    {
        link: {
            margin: theme.spacing(1, 1),
            fontFamily: 'Arial, sans-serif',
        },
    }
));

const AddMenu = (props) => {
    const classes = useStyles();

    return (
        <>
            {props.user &&
                <Grid container direction="row" spacing={1} justify="center">
                    <Grid item>
                        <Button to="/eventsday/add-eventsday" color='primary'
                            variant="outlined" className={classes.link} component={NavLink}>
                            Add new events
                    </Button>
                    </Grid>
                </Grid>}
        </>
    );
};

export default AddMenu;