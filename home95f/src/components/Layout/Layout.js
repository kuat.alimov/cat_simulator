import React, { Fragment } from 'react';
import { Container, makeStyles } from '@material-ui/core';
import Header from '../Header/Header';

const useStyles = makeStyles(() => (
    {
        layoutContent: {
            marginTop: '12px',
        },
    }
))

const Layout = (props) => {
    const classes = useStyles();

    return (
        <Fragment>
            <Container>
                <Header />
                <main className={classes.layoutContent}>
                    {props.children}
                </main>
            </Container>
        </Fragment>
    )
};

export default Layout;