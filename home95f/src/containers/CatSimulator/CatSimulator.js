import React, { useEffect, useMemo } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Container, makeStyles, Grid } from '@material-ui/core';
import Spinner from '../../components/UI/Spinner/Spinner';
import CatInfo from '../../components/UI/CatInfo/CatInfo';
import AddCat from '../../components/UI/AddCat/AddCat';
import {
    fetchCatAction,
} from '../../store/action/catAction';
import PlayMenu from '../../components/UI/PlayMenu/PlayMenu';

const useStyles = makeStyles(theme => (
    {
        blockWrapp: {
            display: 'flex',
        },
        blockImg: {
            display: 'flex',
            width: '330px',
            height: '330px',
            justifyContent: 'center',
        },
        img: {
            display: 'block',
            objectFit: 'contain',
            width: '90%',
        },
        noImg: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'lightgrey',
            marginTop: '5px',
            marginBottom: '5px',
            width: '90%',
        },
        blockInfo: {
            marginTop: '10px'
        },
        blockBody: {
            margin: theme.spacing(1.5),
        },
        infoText: {
            margin: '5px 0',
        },
        blockPublish: {
            display: 'flex',
            flexDirection: 'column',
            margin: theme.spacing(1),
        },
        blockPublishBtn: {
            display: 'flex',
            flexDirection: 'column',
        },
        blockPublishInfo: {
            marginLeft: 'auto',
            margin: theme.spacing(1),
            fontSize: 20,
        },
        publicInfo: {
            margin: '6px auto',
            padding: 0,
            color: 'green',
        },
        unpublicInfo: {
            margin: '6px auto',
            padding: 0,
            color: 'red',
        },
        publishBtn: {
            margin: theme.spacing(1),
        }
    }
));

const CatSimulator = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user)
    const { catAction } = useSelector(state => state.catAction, shallowEqual());
    let userID;

    if (user) {
        userID = user.id;
    }
    useEffect(() => {
        dispatch(fetchCatAction(userID));
    }, [dispatch]);

    const catActionSimulator = useMemo(() => {
        return catAction
    }, [catAction]);

    const catID = useMemo(() => {
        if (catAction.length > 0) {
            return catAction[0]._id
        }
    }, [catAction]);

    let catItem;
    if (catActionSimulator.length > 0) {
        catItem = (
            catActionSimulator.map((item, i) => {
                return (
                    <CatInfo
                        key={i}
                        catName={item.catName}
                        catOld={item.catOld}
                        eatLevel={item.eatLevel}
                        happyLevel={item.happyLevel}
                        avatarCat={item.avatarCat}
                        userId={item.user._id}
                        userName={item.user.displayName}
                    />
                )
            })
        )
    }
    if (catActionSimulator.length === 0) {
        catItem = (
            <>
                <AddCat />
            </>
        )
    }


    return (
        <>
            <Container maxWidth="md" component="main">
                <Grid container direction="column" spacing={1} alignItems="center">
                    <Grid item>
                        {catItem}
                    </Grid>
                    <Grid item>
                        {catAction.length > 0 && <PlayMenu catID={catID} userID={userID} />}
                    </Grid>
                </Grid >
            </Container>
        </>
    )
}

export default CatSimulator;