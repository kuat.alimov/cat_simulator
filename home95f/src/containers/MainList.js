import React from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Grid, Container, makeStyles } from '@material-ui/core';
import Spinner from '../components/UI/Spinner/Spinner';

const useStyles = makeStyles(theme => (
    {
        gridItem: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            margin: theme.spacing(1),
        },
    }
));

const MainList = ({ user }) => {
    const classes = useStyles();

    let loginUser = false;

    const { loading } = useSelector(state => state.catAction, shallowEqual());

    const catSimulatorPage = () => {
        return <Redirect to="/catsimulator" />
    }

    if (user) {
        loginUser = true;
    }

    return (
        <Container maxWidth="md" component="main">
            {loading ? <Spinner /> : null}
            {loginUser ?
                <Grid container direction="column" spacing={2} justify="center">
                    {catSimulatorPage()}
                </Grid>
                :
                <Grid container direction="row" spacing={2} justify="center">
                    <Grid item className={classes.gridItem}>
                        <h1>W E L C O M</h1>
                        <h2>Для начала игры авторизуйтесь</h2>
                        <h2>или зарегистрируйтесь</h2>
                    </Grid>
                </Grid>}
        </Container>
    )
}

export default MainList;