import React from 'react';
import Layout from './components/Layout/Layout';
import { CssBaseline } from '@material-ui/core';
import './App.css';
import { Route, Switch, Redirect } from 'react-router-dom';
import MainList from './containers/MainList';
import { useSelector } from 'react-redux';
import store from './store/configStore';

import Login from './containers/User/Login';
import Register from './containers/User/Register';
import CatSimulator from './containers/CatSimulator/CatSimulator';
import axiosApi from './axios-api';


function App() {
  if (store.getState().users.user) {
    axiosApi.defaults.headers.common['Authorization'] = store.getState().users.user.token;
  }

  const user = useSelector(state => state.users.user);
  return (
    <CssBaseline>
      <Layout>
        <Switch>
          <Route exact path="/" render={() => (<MainList user={user} />)} />
          <ProtectedRoute isAllowed={user !== null} redirectTo={'/login'} exact path="/catsimulator" component={CatSimulator} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route render={() => (<div>not found 404</div>)} />
        </Switch>
      </Layout>
    </CssBaseline>
  );
}

const ProtectedRoute = ({ isAllowed, redirectTo, ...props }) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo} />
};
export default App;
