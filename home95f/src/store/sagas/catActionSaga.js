import { push } from 'connected-react-router';
import { put } from 'redux-saga/effects';
import axios from '../../axios-api';
import {
    catActionRequest, putCatActionData,
    catActionSuccess, catActionError,
} from '../action/catAction';


export function* fetchCatActionSaga({ id }) {
    try {
        yield put(catActionRequest());
        const response = yield axios.get(`/catsimulator?userId=${id}`);
        if (response.data !== null) {
            yield put(putCatActionData(response.data));
            yield put(catActionSuccess());
        }
    } catch (err) {
        yield put(catActionError(err));
    }
};

export function* addNewCatSaga({ dataCopy }) {
    try {
        yield put(catActionRequest());
        yield axios.post('/catsimulator', dataCopy);
        yield put(catActionSuccess());
        yield put(push('/'));
    } catch (e) {
        if (e.response && e.response.data) {
            yield put(catActionError(e.response.data));
        } else {
            yield put(catActionError({ global: 'No internet' }));
        }
    }
};

export function* fetchActionWithCatSaga({ dataCopy }) {
    try {
        yield put(catActionRequest());
        yield axios.post('/catsimulator/cat-action', dataCopy);
        yield put(catActionSuccess());
        yield put(push('/'));
    } catch (e) {
        if (e.response && e.response.data) {
            console.log(e.response.data)
            yield put(catActionError(e.response.data));
            if (e.response.data.result) {
                const resultCopy = [];
                resultCopy.push(e.response.data.result)
                yield put(putCatActionData(resultCopy));
            }
        } else {
            yield put(catActionError({ global: 'No internet' }));
        }
    }
};
