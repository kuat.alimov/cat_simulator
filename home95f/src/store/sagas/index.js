import { takeEvery } from 'redux-saga/effects';
import {
    FACEBOOK_LOGIN, LOGIN_USER,
    LOGOUT_USER, REGISTER_USER
} from '../action/usersAction';
import {
    facebookLoginSaga, loginUserSaga,
    logoutUserSaga, registerUserSaga
} from './userSaga';
import {
    ACTION_WITH_CAT,
    CREATE_CAT,
    FETCH_CAT_ACTION,
} from '../action/catAction';
import {
    addNewCatSaga,
    fetchActionWithCatSaga,
    fetchCatActionSaga,
} from './catActionSaga';


export function* rootSaga() {
    yield takeEvery(REGISTER_USER, registerUserSaga);
    yield takeEvery(LOGIN_USER, loginUserSaga);
    yield takeEvery(FACEBOOK_LOGIN, facebookLoginSaga);
    yield takeEvery(LOGOUT_USER, logoutUserSaga);

    yield takeEvery(FETCH_CAT_ACTION, fetchCatActionSaga);
    yield takeEvery(CREATE_CAT, addNewCatSaga);
    yield takeEvery(ACTION_WITH_CAT, fetchActionWithCatSaga);
}