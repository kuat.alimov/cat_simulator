import { push } from 'connected-react-router';
import { put } from 'redux-saga/effects';
import axios from '../../axios-api';
import {
    registerUserSuccess, registerUserFailure,
    loginUserSuccess, loginUserFailure
} from '../action/usersAction';


export function* registerUserSaga({ userData }) {
    try {
        yield axios.post("/users", userData)
        yield put(registerUserSuccess())
        yield put(push('/'));
    } catch (e) {
        if (e.response && e.response.data) {
            yield put(registerUserFailure(e.response.data));
        } else {
            yield put(registerUserFailure({ global: 'No internet' }));
        }
    }
};

export function* loginUserSaga({ userData }) {
    try {
        const response = yield axios.post("/users/sessions", userData)
        yield put(loginUserSuccess(response.data))
        yield put(push('/'));
    } catch (e) {
        if (e.response && e.response.data) {
            yield put(loginUserFailure(e.response.data));
        } else {
            yield put(loginUserFailure({ global: 'No internet' }));
        }
    }
};

export function* facebookLoginSaga({ data }) {
    try {
        const response = yield axios.post('/users/facebooklogin', data);
        yield put(loginUserSuccess(response.data.user));
        yield put(push('/'));
    } catch (e) {
        if (e.response && e.response.data) {
            yield put(loginUserFailure(e.response.data));
        } else {
            yield put(loginUserFailure({ global: 'No internet' }));
        }
    }
}

export function* logoutUserSaga() {
    yield axios.delete("/users/sessions");
    yield put(push('/'));
}