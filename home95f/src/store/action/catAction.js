export const GET_CAT_ACTION = 'GET_CAT_ACTION';
export const CAT_ACTION_REQUEST = 'CAT_ACTION_REQUEST';
export const CAT_ACTION_SUCCESS = 'CAT_ACTION_SUCCESS';
export const CAT_ACTION_ERROR = 'CAT_ACTION_ERROR';

export const FETCH_CAT_ACTION = 'FETCH_CAT_ACTION';
export const CREATE_CAT = 'CREATE_CAT';

export const ACTION_WITH_CAT = 'ACTION_WITH_CAT';

export const putCatActionData = (value) => ({ type: GET_CAT_ACTION, value });

export const catActionRequest = () => ({ type: CAT_ACTION_REQUEST });
export const catActionSuccess = () => ({ type: CAT_ACTION_SUCCESS });
export const catActionError = (error) => ({ type: CAT_ACTION_ERROR, error });

export const fetchCatAction = (id) => ({ type: FETCH_CAT_ACTION, id });

export const fetchActionWithCat = (dataCopy) => ({ type: ACTION_WITH_CAT, dataCopy });

export const addNewCat = (dataCopy) => ({ type: CREATE_CAT, dataCopy });
