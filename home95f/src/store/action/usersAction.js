
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const REGISTER_USER = 'REGISTER_USER';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGIN_USER = 'LOGIN_USER';

export const LOGOUT_USER = 'LOGOUT_USER';

export const FACEBOOK_LOGIN = 'FACEBOOK_LOGIN';


export const registerUserSuccess = () => ({ type: REGISTER_USER_SUCCESS });

export const registerUserFailure = (err) => ({ type: REGISTER_USER_FAILURE, err });

export const registerUser = (userData) => ({ type: REGISTER_USER, userData });

export const logoutUserSuccess = () => {
    return { type: LOGOUT_USER }
}

export const logoutUser = () => ({ type: LOGOUT_USER });

export const loginUserSuccess = (user) => {
    return { type: LOGIN_USER_SUCCESS, user }
}

export const loginUserFailure = (err) => {
    return { type: LOGIN_USER_FAILURE, err }
}

export const loginUser = (userData) => ({ type: LOGIN_USER, userData });

export const facebookLogin = (dataCopy) => ({ type: FACEBOOK_LOGIN, dataCopy });