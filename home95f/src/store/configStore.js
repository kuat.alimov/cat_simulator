import { createStore, applyMiddleware, compose, combineReducers } from 'redux';

import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';

import catReducer from './reducer/catReducer';
import usersReducer from './reducer/userReducer';
import { rootSaga } from './sagas/index';

export const history = createBrowserHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    catAction: catReducer,
    users: usersReducer,
    router: connectRouter(history),
});

const sagaMiddleware = createSagaMiddleware();

const middleWare = [
    routerMiddleware(history),
    sagaMiddleware,
];

const saveToLocalStorage = state => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    } catch (e) {
        console.log('Error save state');
    }
}

const loadLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (e) {
        return undefined;
    }
}

const enhancers = composeEnhancers(applyMiddleware(...middleWare));
const persistedState = loadLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

sagaMiddleware.run(rootSaga);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user,
        }
    })
})

export default store;