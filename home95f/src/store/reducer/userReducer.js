import {
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS,
    LOGOUT_USER
} from "../action/usersAction";

const initState = {
    registerError: null,
    loginError: null,
    user: null,
}

const usersReducer = (state = initState, action) => {
    switch (action.type) {
        case REGISTER_USER_SUCCESS:
            return { ...state, registerError: null };
        case REGISTER_USER_FAILURE: {
            return { ...state, registerError: action.err }
        }
        case LOGIN_USER_SUCCESS:
            return { ...state, user: action.user, loginError: null };
        case LOGIN_USER_FAILURE:
            return { ...state, loginError: action.err };
        case LOGOUT_USER:
            return { ...state, user: null };
        default:
            return state;
    }
}

export default usersReducer;