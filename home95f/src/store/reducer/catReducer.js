import {
    GET_CAT_ACTION,
    CAT_ACTION_REQUEST,
    CAT_ACTION_SUCCESS,
    CAT_ACTION_ERROR,
} from '../action/catAction';

const initialState = {
    catAction: [],
    loading: false,
    error: null,
};

const catReducer = (state = initialState, action) => {
    switch (action.type) {
        case CAT_ACTION_REQUEST:
            return { ...state, loading: true };
        case CAT_ACTION_SUCCESS:
            return { ...state, loading: false, error: null };
        case CAT_ACTION_ERROR:
            return { ...state, loading: false, error: action.error };
        case GET_CAT_ACTION:
            return { ...state, catAction: action.value };
        default:
            return state;
    };
};

export default catReducer;