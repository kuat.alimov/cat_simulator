import axios from 'axios';
import { apiURL } from './apiUrl';

const instance = axios.create({
    baseURL: apiURL
});

export default instance;